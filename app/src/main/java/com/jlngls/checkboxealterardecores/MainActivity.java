package com.jlngls.checkboxealterardecores;

import androidx.appcompat.app.AppCompatActivity;

import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.CheckBox;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private CheckBox checkAzul, checkVermelho, checkVerde;
    private TextView resultado;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        checkAzul           = findViewById(R.id.checkAzul);
        checkVermelho       = findViewById(R.id.checkVermelho);
        checkVerde          = findViewById(R.id.checkVerde);
        resultado           = findViewById(R.id.resultado);
    }

    public void checkBox() {

        String texto = "";

        if (checkAzul.isChecked()) {


            String corSelecionada = checkAzul.getText().toString();  // buscar texto inseridos na caixa de texto
            texto = corSelecionada;

            checkAzul.setTextColor(Color.parseColor("#0055ff"));  // mudar do de texto.
            resultado.setBackgroundColor(Color.parseColor("#12235e")); // mudar cor de fundo.

        }

        if (checkVerde.isChecked()) {

            String corSelecionada = checkVerde.getText().toString();
            texto = texto + corSelecionada;

            checkVerde.setTextColor(Color.parseColor("#72ff00"));
            resultado.setBackgroundColor(Color.parseColor("#327527"));

        }
        if (checkVermelho.isChecked()) {
            String corSelecionada = checkVermelho.getText().toString();
            texto = texto + corSelecionada;


            checkVermelho.setTextColor(Color.parseColor("#ff3200"));
            resultado.setBackgroundColor(Color.parseColor("#752732"));
        }
        resultado.setText(texto);


        /// meus teste, voltando cor no original do checkbox

        if (!checkAzul.isChecked()) {
            checkAzul.setTextColor(Color.parseColor("#000000"));

        }
        if (!checkVerde.isChecked()) {
            checkVerde.setTextColor(Color.parseColor("#000000"));

        }
        if (!checkVermelho.isChecked())  {
            checkVermelho.setTextColor(Color.parseColor("#000000"));

        }
        if(!checkVermelho.isChecked() && !checkVerde.isChecked() && !checkAzul.isChecked()){
            resultado.setBackgroundColor(Color.parseColor("#ffffff"));
        }

    }

    public void onClick(View view){
        checkBox();
    }
}
